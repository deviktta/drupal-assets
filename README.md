# Web Dev Docs

## Software documentation
- [Behat](./behat): PHP test framework
- [Drupal](./drupal-10): PHP CMS
- [Git](./git): Version Control System
- [NextJS](./nextjs-13): Javascript framework
- [PHP](./php): Programming language
- [Symfony](./symfony-6): PHP framework

## Main concepts
- [CI / CD / CD](#ci--cd--cd)

### CI / CD / CD

- **Continuous integration (CI)** is the practice of integrating source code changes frequently and ensuring that the integrated codebase is in a workable state.
- **Continuous delivery (CD)** is a software engineering approach in which teams produce software in short cycles, ensuring that the software can be reliably released at any time.
- **Continuous deployment (CD)** is a software engineering approach in which software functionalities are delivered frequently and through automated deployments.  

![CI / CD / CD](./ci-cd-cd.webp)  
_Image source: [SKYLYT IT @ Medium.com](https://medium.com/@skylytit.com/from-commit-to-deployment-the-ci-cd-journey-demystified-89629c9bcb06)_