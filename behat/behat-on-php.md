# Behat on PHP

## Installation

- Download via Composer the last version of `behat/behat` package.


## Behat commands

- `vendor/bin/behat --init`: Creates the directory and files structure.
  - `features/`: .features files
  - `features/bootstrap/`: for context classes
  - `features/bootstrap/FeatureContext.php`: file to place definitions, transformations and hooks.
    - Steps and scenarios literals become camel case method with their corresponding annotation `@Given @When @Then`.
    - We can store data in class properties to reuse it later.
    - Variables in method arguments must match PhpDoc bind patterns; their data type will always be string.

- `vendor/bin/behat`: runs scenarios and tests.


## Feature: example

```gherkin
Feature: List of announcements
  Scenario: I was a list of announcements
    Given I am an authenticated user
    When I request a list of announcements from "http://localhost"
    Then the results should include an announcement with ID "1234"
```

Running `vendor/bin/behat` will detect undefined scenario and steps and will ask to create method stubs for us.


## FeatureContext: example

```php
/**
 * @When I do something with :methodArgument
 */
public function someMethod($methodArgument) {}
```

All argument data types are always string.


## Advanced usage

*Hard-coded data from the feature file.*

```gherkin
Feature: Users

  Scenario: Creating Users
    Given the following users:
      | name          | followers |
<<<<<<< HEAD
      | alice         | 147       |
      | bob           | 142       |
      | charlie       | 274       |
      | dave          | 962       |
=======
      | everzet       | 147       |
      | avalanche123  | 142       |
      | kriswallsmith | 274       |
      | fabpot        | 962       |
>>>>>>> 953459663e96eb682156636f49a68ccdacde5090
```

```php
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;

class FeatureContext implements Context
{
    /**
     * @Given the following users:
     */
    public function pushUsers(TableNode $usersTable)
    {
        $users = array();
        foreach ($usersTable as $userHash) {
            $user = new User();
            $user->setUsername($userHash['name']);
            $user->setFollowersCount($userHash['followers']);
            $users[] = $user;
        }

        // do something with $users
    }
```