# Debian 11

## Software list

| Name              | Type                     | Method                                      | Method link                                      |
|-------------------|--------------------------|---------------------------------------------|--------------------------------------------------|
| **Balena Etcher** | OS images tool           | Website download - _.deb_ file              | https://etcher.balena.io/                        |
| **Dbeaver CE**    | Database tool            | Website download - _.deb_ file              | https://dbeaver.io/download/                     |
| **Dia**           | Diagram creation         | Apt                                         | http://dia-installer.de                          |
| **Filezilla**     | FTP tool                 | Apt                                         | https://filezilla-project.org                    |
| **Firefox ESR**   | Browser                  | Apt                                         | https://www.mozilla.org/en-US/firefox            |
| **Firefox DE**    | Browser                  | Website download - standalone app           | https://www.mozilla.org/en-US/firefox/developer/ |
| **Flameshot**     | Screenshot tool          | Apt                                         | https://flameshot.org                            |
| **Ghostwriter**   | Text editor for Markdown | Apt                                         | https://ghostwriter.kde.org/                     |
| Git Kraken        | Git GUI                  | Website download - _.deb_ file              | https://www.gitkraken.com/download               |
| Google Chrome     | Browser                  | Website download - _.deb_ file              | https://www.google.com/chrome/                   |
| JetBrains Toolbox | IDE                      | Website download - installation file        | https://www.jetbrains.com/toolbox-app/           |
| **LibreOffice**   | Office suite             | Apt                                         | https://www.libreoffice.org/                     |
| **Piper**         | Hardware utility         | Apt                                         | https://github.com/libratbag/piper               |
| Postman           | API client               | Website download - standalone app           | https://www.postman.com/downloads/               |
| Slack             | Communication            | Website download - _.deb_ file              | https://slack.com/downloads/linux                |
| Spotify           | Spotify client           | Website configuration - custom _apt_ source | https://www.spotify.com/es/download/linux/       |
| Screaming Frog    | Website crawler          | Website download - _.deb_ file              | https://www.screamingfrog.co.uk/seo-spider/      |
| **VLC**           | Video player             | Apt                                         | https://www.videolan.org/vlc                     |

_In bold, Open Source Software._