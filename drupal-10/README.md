# Drupal 10 assets


## Documentation

* [Drupal 10 overview](docs/drupal-10-overview.md)
* [Links](docs/links.md)

**Basics:**
* [Drush](docs/basics/drush.md)
* [List of well-known modules](docs/basics/list-of-well-known-modules.md)
* [MySQL](docs/basics/mysql.md)
* [Security checks](docs/basics/security-checks.md)
* [Site building best practices strategy](docs/basics/site-building-best-practices-strategy.md)
* [Troubleshooting](docs/basics/troubleshooting.md)

**Extending Drupal:**
* [Cache - Redis as Drupal cache](docs/extending-drupal/cache--redis-as-drupal-cache.md)
* [Code review - PHPStan](docs/extending-drupal/code-review--phpstan.md)
* [Code review - PHP CodeSniffer](docs/extending-drupal/code-review--php-codesniffer.md)
* [Drupal - Menu API](./docs/extending-drupal/drupal--menu-api.md)
* [Drupal - Tokens](docs/extending-drupal/drupal--tokens.md)
* [E-mail - Sending e-mails with Drupal](docs/extending-drupal/email--sending-emails-with-drupal.md)
* [Search - Search API](docs/extending-drupal/search--search-api.md)
* [Twig extension - Twig Tweak](docs/extending-drupal/twig--extension-twig-tweak.md)

**Local development**
* [Drupal project setup](docs/local-development/drupal-project-setup)
* [DDEV](docs/local-development/ddev.md)
* [Docksal](docs/local-development/docksal.md)


## Scaffold files

* [Scaffolding files](scaffold-files)


## Snippets

* [Drupal 10 - my_module.routing.yml](snippets/drupal-10-my_module.routing.yml.md)
