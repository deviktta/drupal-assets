# Basics - Drush

**Contents:**
- [Directories and discovery](#directories-and-discovery)
- [Commands: Drupal core](#commands-drupal-core)
- [Commands: Acquia Cloud Site Factory](#commands-acquia-cloud-site-factory)
- [Commands: Acquia Site Studio](#commands-acquia-site-studio)
- [Commands: Acquia Cohesion (product renamed to Acquia Site Studio)](#commands-acquia-cohesion-product-renamed-to-acquia-site-studio)


## Directories and discovery
If a configuration file is found in any of the below locations, it will be loaded and merged with other configuration files in the search list. Run `drush status --fields=drush-conf` to see all discovered config files.

`drush.yml` files are discovered as below, in order of precedence:
1. `sites/<DRUPAL_SITE>/drush.yml`
2. `sites/all/drush`, `<WEBROOT>/drush` or `<PROJECTROOT>/drush`
3. In any location, as specified by the --config option.
4. User's .drush folder (i.e. ~/.drush/drush.yml).
5. System-wide configuration folder (e.g. /etc/drush/drush.yml).


## Commands: Drupal core
- Clearing cache: 
  ```
  drush cr
  ```
- Update database: 
  ```
  drush updb
  ```
  
- Get admin autologin link: 
  ```
  drush uli
  ```
  
- Import database: 
  ```
  drush sql:cli < drupal-root/web/database-backup.sql
  ```

- Export database: 
  ```
  drush sql:dump --result-file=database-backup.sql [--gzip]
  ```

- Copy database to one site to another: 
  ```
  drush sql:sync @origin @destination [--sanitize]
  ```

- Copy static files to one site to another:
  ```
  ddev drush rsync @origin:%files/subdirectory subdirectory
  ```

- Import translations: 
  ```
  drush locale-import nl nl.po --type=customized --override=all
  ```

- Get _config_ by language: 
  ```
  drush config:get language.fr-ca:system.site --include-overridden
  ```


## Commands: Acquia Cloud Site Factory
- `drush @mywebsite.env sfl`: lists details for all sites in the factory.
- `drush @mywebsite.env sfi`: lists site specific information for all sites in the factory.
- `drush @mywebsite.env sfml`: runs any drush command against all sites in the factory.
- `drush @mywebsite.env sfdu`: creates database backups for all sites in the factory.
- `drush @mywebsite.env sfre`: restores database backups for all sites in the factory.
- `drush @mywebsite.env sfa`: extracts information about modules, themes, entities, views from sites in the factory.
- It's also possible to work with Drush with a specific site by using the `--uri` option with the targeted site.


## Commands: Acquia Site Studio
- Import Site Studio package from sync: `drush sitestudio:package:import --path=../config/sitestudio/`
  - (Mostly probable) Import command also runs a Site Studio rebuild.
- Export Site Studio package files to a path: `drush sitestudio:package:export --path=../config/sitestudio/`


### Commands: Acquia Cohesion (product renamed to Acquia Site Studio)
- Import: `drush dx8:import` renamed into `drush cohesion:import`
- Rebuild: `drush dx8:rebuild` renamed into `drush cohesion:rebuild`

**Old Cohesion commands:**
- Import Site Studio assets from the API: `drush cohesion:import`
- Rebuild Site Studio (re-saves settings, styles and templates): `drush cohesion:rebuild`
