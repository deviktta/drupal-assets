# Basics - List of well-known modules

**Contents:**
- [Analytics & marketing, content management & templating, forms, user and SEO](#analytics--marketing-content-management--templating-forms-user-and-seo-)
  - [Analytics & marketing](#analytics--marketing)
  - [Content management & templating](#content-management--templating)
  - [Forms](#forms)
  - [User](#user)
  - [SEO](#seo)

- [Admin UI, e-mail, security, performance, quality code & testing and miscellaneous](#admin-ui-e-mail-performance-quality-code--testing-security-and-miscellaneous)
  - [Admin UI](#admin-ui)
  - [E-mail](#e-mail)
  - [Performance](#performance)
  - [Quality code & testing](#quality-code--testing)
  - [Security](#security)
  - [Miscellaneous](#miscellaneous)


## Analytics & marketing, content management & templating, forms, user and SEO:

### Analytics & marketing
| Name                                                            | Stable release with<br/>Drupal 10 compatibility | Description                                |
|-----------------------------------------------------------------|-------------------------------------------------|--------------------------------------------|
| [Google Tag Manager](https://www.drupal.org/project/google_tag) | Yes                                             | Integrates Google Tag Manager application. |


### Content management & templating
| Name                                                                                          | Stable release with<br/>Drupal 10 compatibility | Description                                                                                                                                |
|-----------------------------------------------------------------------------------------------|-------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
| [Animated Scroll To Top](https://www.drupal.org/project/animated_scroll_to_top)               | Yes                                             | Provides a smooth page scrolling effect for returning to the top of the page using jQuery.                                                 |
| [Better Exposed Filters](https://www.drupal.org/project/better_exposed_filters)               | Yes                                             | Extends Drupal core Views with more options for filters.                                                                                   |
| [Content locking (anti-concurrent editing)](https://www.drupal.org/project/content_lock)      | Yes                                             | Anti-concurrent editing.                                                                                                                   |
| [Default content deploy](https://www.drupal.org/project/default_content_deploy)               | Yes                                             | Provides content acress environments.                                                                                                      |
| [Easy Breadcrumb](https://www.drupal.org/project/easy_breadcrumb)                             | Yes                                             | Extends Drupal core breadcrumb.                                                                                                            |
| [Entity Browser](https://www.drupal.org/project/entity_browser)                               | Yes                                             | Provides a generic entity browser/picker.                                                                                                  |
| [Entity Clone](https://www.drupal.org/project/entity_clone)                                   | No (last commit late 2022)                      | Provides the ability to clone entities such as nodes, blocks, files...                                                                     |
| [EU Cookie Compliance (GDPR Compliance)](https://www.drupal.org/project/eu_cookie_compliance) | Yes (rework on-going)                           | Provides a highly customizable Cookie pop-up compliant with GDPR.                                                                          |
| [Field Group](https://www.drupal.org/project/field_group)                                     | Yes                                             | Includes a new form element to group fields within UI.                                                                                     |
| [Hreflang](https://www.drupal.org/project/hreflang)                                           | Yes                                             | Auto includes hreflangs based on current site internationalization setup.                                                                  |
| [Inline Entity Form](https://www.drupal.org/project/inline_entity_form)                       | No (last commit late 2022)                      | Provides a widget for inline management (creation, modification, removal) of referenced entities.                                          |
| [Language Hierarchy](https://www.drupal.org/project/language_hierarchy)                       | No (last commit 2021)                           | Ability to order languages hierarchically to reuse language translations.                                                                  |
| [Link Attributes widget](https://www.drupal.org/project/link_attributes)                      | Yes                                             | Extends Drupal core link field with addition attributes (target, rel and class)                                                            |
| [Menu Link Attributes](https://www.drupal.org/project/menu_link_attributes)                   | Yes                                             | Allows you to add attributes to your menu links or their wrapping <li> elements.                                                           |
| [Metatag](https://www.drupal.org/project/metatag)                                             | Yes                                             | Provides a highly customizable way to modify meta tags (structured data).                                                                  |
| [Node Revision Delete](https://www.drupal.org/project/node_revision_delete)                   | No (last commit early 2023)                     | Allows you track and prune old revisions of content types.                                                                                 |
| [Pathauto](https://www.drupal.org/project/pathauto)                                           | Yes                                             | Customizable automatic generation of URL/path for content.                                                                                 |
| [Paragraphs](https://www.drupal.org/project/paragraphs)                                       | Yes                                             | New entity to group fields, meant to be reusable across different content-types.                                                           |
| [Rabbit Hole](https://www.drupal.org/project/rabbit_hole)                                     | Yes                                             | Perhaps you have a content type that never should be displayed on its own page, like an image content type that's displayed in a carousel. |
| [Search API](https://www.drupal.org/project/search_api)                                       | Yes                                             | Decouples search sources to allow any kind of search engine.                                                                               |
| [Select 2](https://www.drupal.org/project/select2)                                            | Yes                                             | Integrates Drupal autocomplete and select fields with the Select2 jQuery library.                                                          |
| [Twig Tweak](https://www.drupal.org/project/twig_tweak)                                       | Yes                                             | Extends Drupal core's Twig implementation.                                                                                                 |
| [Views Infinite Scroll](https://www.drupal.org/project/views_infinite_scroll)                 | Yes                                             | Extends Drupal core views to have a pager a _infinite scroll_.                                                                             |
| [Webform](https://www.drupal.org/project/webform)                                             | No (_official_, last commit early 2023)         | Highly customizable forms from the admin UI.                                                                                               |


### Forms
| Name                                                                   | Stable release with<br/>Drupal 10 compatibility | Description                                                        |
|------------------------------------------------------------------------|-------------------------------------------------|--------------------------------------------------------------------|
| [Antibot](https://www.drupal.org/project/antibot)                      | Yes                                             | Lightweight module designed to eliminate robotic form submissions. |
| [Field Group](https://www.drupal.org/project/field_group)              | Yes                                             | Includes a new form element to group fields within UI.             |
| [Honey Pot](https://www.drupal.org/project/honeypot)                   | Yes                                             | Protect form with a honeypot.                                      |
| [No Request New Password](https://www.drupal.org/project/noreqnewpass) | Yes                                             | Remove "Request new password" link from block and user page.       |
| [Webform](https://www.drupal.org/project/webform)                      | No (_official_, last commit early 2023)         | Highly customizable forms from the admin UI.                       |


### User
| Name                                                                   | Stable release with<br/>Drupal 10 compatibility | Description                                                                        |
|------------------------------------------------------------------------|-------------------------------------------------|------------------------------------------------------------------------------------|
| [Masquerade](https://www.drupal.org/project/masquerade)                | No (rc available for D10)                       | Impersonates (by role) any user.                                                   |
| [No Request New Password](https://www.drupal.org/project/noreqnewpass) | Yes                                             | Remove "Request new password" link from block and user page.                       |
| [Persistent Login](https://www.drupal.org/project/persistent_login)    | Yes                                             | The Persistent Login module provides a "Remember Me" option on the user login form |


### SEO
| Name                                                                | Stable release with<br/>Drupal 10 compatibility | Description                                                               |
|---------------------------------------------------------------------|-------------------------------------------------|---------------------------------------------------------------------------|
| [Hreflang](https://www.drupal.org/project/hreflang)                 | Yes                                             | Auto includes hreflangs based on current site internationalization setup. |
| [Metatag](https://www.drupal.org/project/metatag)                   | Yes                                             | Provides a highly customizable way to modify meta tags (structured data). |
| [RobotsTxt](https://www.drupal.org/project/robotstxt)               | Yes                                             | Administrate from back-office the contents of robots.txt file.            |
| [Simple XML sitemap](https://www.drupal.org/project/simple_sitemap) | Yes                                             | Integrates and customizes sitemap.xml file.                               |


## Admin UI, e-mail, performance, quality code & testing, security and miscellaneous

### Admin UI
| Name                                                                          | Stable release with<br/>Drupal 10 compatibility | Description                                                                            |
|-------------------------------------------------------------------------------|-------------------------------------------------|----------------------------------------------------------------------------------------|
| [Admin Dialogs](https://www.drupal.org/project/admin_dialogs)                 | Yes                                             | Admin context: ability to add dialogs.                                                 |
| [Admin Toolbar](https://www.drupal.org/project/admin_toolbar)                 | Yes                                             | Admin context: improves Drupal core admin toolbar.                                     |
| [Coffee](https://www.drupal.org/project/coffee)                               | Yes                                             | Admin context: pressing ALT+D opens a search bar with in-line results for admin pages. |
| [Environment Indicator](https://www.drupal.org/project/environment_indicator) | Yes                                             | Admin context: displays a bar specifying current environment name.                     |


### E-mail
| Name                                                                   | Stable release with<br/>Drupal 10 compatibility | Description                                                                                                     |
|------------------------------------------------------------------------|-------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
| [Drupal Symfony Mailer](https://www.drupal.org/project/symfony_mailer) | No (_official_, last commit early 2023)         | A new mail-system based on the popular Symfony Mailer library.                                                  |
| [Reroute Email](https://www.drupal.org/project/reroute_email)          | Yes                                             | Intercepts all outgoing emails from a Drupal site and reroutes them to a predefined configurable email address. |
| [SMTP](https://www.drupal.org/project/smtp)                            | Yes                                             | Integrates SMTP protocol to send e-mails.                                                                       |


### Performance
| Name                                                                    | Stable release with<br/>Drupal 10 compatibility | Description                                                              |
|-------------------------------------------------------------------------|-------------------------------------------------|--------------------------------------------------------------------------|
| [Memcache API and Integration](https://www.drupal.org/project/memcache) | Yes                                             | Integrates Memcache PHP module.                                          |
| [Purge](https://www.drupal.org/project/purge)                           | Yes                                             | Facilitates cleaning external caching systems, reverse proxies and CDNs. |
| [Redis](https://www.drupal.org/project/redis)                           | Yes                                             | Integrates Redis as Drupal cache.                                        |


### Quality code & testing
| Name                                          | Stable release with<br/>Drupal 10 compatibility | Description                   |
|-----------------------------------------------|-------------------------------------------------|-------------------------------|
| [Coder](https://www.drupal.org/project/coder) | Yes                                             | PHPCS integration for Drupal. |


### Security
| Name                                                                                              | Stable release with<br/>Drupal 10 compatibility | Description                                                                                                                 |
|---------------------------------------------------------------------------------------------------|-------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------|
| [Automated Logout](https://www.drupal.org/project/autologout)                                     | Yes                                             | Automated logout after specific time.                                                                                       |
| [ClamAV](https://www.drupal.org/project/clamav)                                                   | Yes                                             | Drupal integration with the open-source ClamAV virus scanner.                                                               |
| [Flood control](https://www.drupal.org/project/flood_control)                                     | Yes                                             | Provides an interface for hidden flood control variables.                                                                   |
| [Honey Pot](https://www.drupal.org/project/honeypot)                                              | Yes                                             | Protect form with a honeypot.                                                                                               |
| [Login Disable](https://www.drupal.org/project/login_disable)                                     | Yes                                             | Prevent users from logging in to your Drupal site unless they know the secret key to add to the end of the login form page. |
| [Login Security](https://www.drupal.org/project/login_security)                                   | Yes                                             | Improves the security options in the login operation.                                                                       |
| [No Request New Password](https://www.drupal.org/project/noreqnewpass)                            | Yes                                             | Remove "Request new password" link from block and user page.                                                                |
| [Password Policy](https://www.drupal.org/project/password_policy)                                 | Yes                                             | Enforces password definition with policies.                                                                                 |
| [Restrict IP](https://www.drupal.org/project/restrict_ip)                                         | Yes                                             | This module allows administrators to restrict access to the site to an administrator defined set of IP addresses            |
| [Restrict route by IP](https://www.drupal.org/project/restrict_route_by_ip)                       | Yes                                             | Provides an interface to manage route restriction by IP address.                                                            |
| [Security kit](https://www.drupal.org/project/seckit)                                             | Yes                                             | Includes HTTP headers control.                                                                                              |
| [Shield](https://www.drupal.org/project/shield)                                                   | Yes                                             | Protects the website under a htaccess password.                                                                             |
| [Username Enumeration Prevention](https://www.drupal.org/project/username_enumeration_prevention) | Yes                                             | Mitigate common ways of anonymous users identifying valid usernames on a Drupal site.                                       |



### Miscellaneous
| Name                                                                                 | Stable release with<br/>Drupal 10 compatibility | Description                                           |
|--------------------------------------------------------------------------------------|-------------------------------------------------|-------------------------------------------------------|
| [MySQL 5.6 and MariaDB 10.0 database driver](https://www.drupal.org/project/mysql56) | Yes                                             | Drivers for old version of MySQL 5.6 or Mariadb 10.0. |
