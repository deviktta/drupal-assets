# Basics - MySQL

- [MySQL](#MySQL)
  - [Import and export database](#import-and-export-database)


## MySQL

### Import and export database
- Import database: `mysql -u user -ppass dbname < dump.sql`
- Export database: `mysqldump -u user -ppass dbname > dump.sql`

