# Basics - Security checks

- [ ] Disable development modules.

- [ ] Deactivate any development settings that should not be used on the production.

- [ ] Logs should be sent to syslog instead of DBlog (if applicable).

- [ ] Modules with security fix releases should be updated to the latest version.

- [ ] Drupal core should be kept up to date.

- [ ] Strong admin and user password policy.

- [ ] Remove SQL dumps.

- [ ] Set up the correct file and directory tree permissions (attention to private folder).

- [ ] If necessary, user registration should be disabled.

- [ ] Public forms should have a captcha enabled.

- [ ] The website should be configured with SSL.

- [ ] The website should be protected against HTTP HOST header attacks through `$settings['trusted_host_patterns']`.

- [ ] Make sure that security headers are set to protect, using Sec Kit among others.
