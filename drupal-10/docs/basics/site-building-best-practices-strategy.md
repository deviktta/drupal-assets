# Basics - Site building best practices strategy

- [ ] Configure Drush: Drush sites + local _drush.yml_ file.

- [ ] Implement Drupal multisite: One site per environment (_local_, _prod_, ...).

- [ ] Improve cache performance with [Redis as Drupal cache](../extending-drupal/cache--redis-as-drupal-cache.md).

- [ ] Index content with Search API, implementing high-performance solutions like Apache Solr when possible.

- [ ] Install and configure Security Kit module.

- [ ] Quality code tools: PHP_CodeSniffer + PHPStan.
