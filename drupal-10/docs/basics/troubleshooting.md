# Basics - Troubleshooting


**ERROR #1: The module does not exist during**

Drupal displays the following error message: `The <MODULE_NAME> does not exist`

1. Delete record from `key_value` database table:
```php
\Drupal::service('keyvalue')->get('system.schema')->delete('MODULE_NAME');
```

2. Delete the record from active config:
```shell
drush config:del core.extension module.<MODULE_NAME>
```
