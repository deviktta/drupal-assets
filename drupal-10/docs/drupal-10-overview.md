# Drupal 10 overview

## Highlights

- Themes:
  - New administration theme: Claro (replacing [Seven](https://www.drupal.org/project/seven), available as a contributed theme).
  - New default theme: Olivero (replacing [Bartik](https://www.drupal.org/roject/bartik), available as a contributed theme).
  - Drupal 9 core themes moved to contrib themes: [Classy](https://www.drupal.org/project/classy) and [Stable](https://www.drupal.org/project/stable).
  - New base theme [stable9](https://www.drupal.org/node/3107179).
  - New sub-themes must be built using [Starterkit](https://www.drupal.org/docs/theming-drupal/creating-sub-themes), inheriting the new base theme stable9, or create a new theme from scratch.
- Introducing CKEditor 5 (replacing CKEditor 4).
- Introducing modern Javascript components (rather than Jquery).
- Featuring Symfony 6 (and PHP 8.1 as requirement).

**Other changes**:
- SimpleTest removed from the core test runner; testing must be done with PHPUnit.
- Ajax commands can now return promises.
- Hook hook_entity_view_mode_alter definition has removed $context argument.
- Permissions must exist: https://www.drupal.org/node/3193348.
- PHP dependencies:
  - guzzlehttp/guzzle to 7.5
  - asm89/stack-cors to 2.1.1
  - Twig to 3.4.3
- Front-end dependencies:
  - Jquery to 3.6.2
- Development dependencies:
  - Node.js 16 is now a dependency for Drupal core. 
  - PHPStan added to Drupal's core development dependencies.
  - PHPUnit to 9.5.24.
- Coding standards:
  - New coding standards have been enabled in core.
  - All YAML files are linted for correct indentation.

## Upgrading from Drupal 9.x to 10

- Steps to upgrade:
  - Update to PHP 8.1.
  - Update Drupal to 9.5 or greater.
  - Update contributed projects.
    - [Upgrade status](https://www.drupal.org/project/upgrade_status).
  - Update custom projects.
    - Check deprecations from Drupal 10; quality code tools, IDE support and [Upgrade status](https://www.drupal.org/project/upgrade_status) contrib module will help.
  - Upgrade to Drupal 10 and run database updates.

## Links

- [About Drupal 10 | Drupal.org](https://www.drupal.org/about/10)
- [Release drupal 10.0.0 | Drupal.org](https://www.drupal.org/project/drupal/releases/10.0.0)
- [Upgrading from Drupal 9 to Drupal 10 | Drupal.org](https://www.drupal.org/docs/upgrading-drupal/upgrading-from-drupal-8-or-later/upgrading-from-drupal-9-to-drupal-10-0)