# Extending Drupal - Cache - Redis as Drupal cache

_This article was written for Drupal 9; to be tested in Drupal 10._

**Contents:**
1. Get Redis
2. Integrate Redis with Drupal
   1. Get the Drupal module for Redis
   2. Configure the Redis module
   3. Using our Redis as cache

Step 1: Get Redis
-----------------
This Docker template is shipped with Redis as a service. Best is to check if Redis is up & running:
1. Get the Redis host, usually by finding the Docker container id through `docker ps` and then getting the IP by running `docker inspect <CONTAINER_ID> | grep IPAddress`.
2. `redis-cli -h <REDIS_HOST|redis>`
3. `redis:6379> AUTH <REDIS_USERNAME|default> <REDIS_PASSWORD|redis>`
4. `redis:6379> INFO`

At this point, Redis is available in our Docker stack.

Step 2: Integrate Redis with Drupal
----------------------------------
First, get the [Drupal module for Redis](https://www.drupal.org/project/redis):
```
# Get the Composer package
composer require 'drupal/redis:^1.5'

# Enable the Drupal module
drush pm:enable redis
```
Then, configure the Redis module by modifying your `local.settings.php`. 

_Official README available at `modules/contrib/redis/README.md`._

1. Select the Redis interface is to use:
- The recommended way is to have installed the Redis module installed in your current PHP build:

`$settings['redis.connection']['interface'] = 'PhpRedis';`

- As an alternative, you can install Predis package `composer require predis/predis` and select its interface:

`$settings['redis.connection']['interface'] = 'Predis';`

2. Add the connection details:
```
$settings['redis.connection']['host'] = 'redis';
$settings['redis.connection']['password'] = 'redis';
```


Step 3: Using Redis as Drupal cache
----------------------------------
At this point, Redis is configured and connected with Drupal. Let's tell Drupal to use Redis as their caches.

- Use for all bins:

`$settings['cache']['default'] = 'cache.backend.redis';`

- Use it for specific cache bins:

`$settings['cache']['bins']['render'] = 'cache.backend.redis';`

Additionals checks:
- Checking Redis usage at the Drupal report page: `/admin/reports/redis`

- Checking that the corresponding Drupal database tables `cache_<bin>` are no longer in use.