# Extending Drupal - Code review - PHP CodeSniffer

* PHP CodeSniffer documentation: https://github.com/squizlabs/PHP_CodeSniffer
* Drupal documentation for PHP CodeSniffer: https://www.drupal.org/docs/contributed-modules/code-review-module/php-codesniffer-command-line-usage

**Contents:**
1. [Installation](#step-1--installation)
2. [Configuration](#step-2--configuration)
3. [Run](#step-3--run)
4. [(Optional) Integration for PhpStorm](#-optional--integration-for-phpstorm)
5. [(Optional) Integration for Git hooks](#-optional--integration-for-git-hooks)


Step 1: Installation
--------------------

To install PHP CodeSniffer, Drupal has the contributed module [drupal/coder](https://www.drupal.org/project/coder/) that takes care of the installation of PHP CodeSniffer and includes Drupal rules for it:
```shell
composer require --dev drupal/coder
```

Only the Composer package is required, do not install the module since it's meant to only provides CLI integration for PHP CodeSniffer.


Step 2: Configuration
---------------------

Create a new file inside the Drupal project root directory, name it `phpcs.xml` and configure it with your needs:
* Sample file: [phpcs.dist.xml](../../scaffold-files/phpcs.xml.dist)
* PHP CodeSniffer standard file name for distribution: `phpcs.dist.xml`


Step 3: Run
-----------

Now, PHP CodeSniffer will detect automatically our `phpcs.xml` file; it's enough by running PHPStan from the CLI:
```shell
# Review code with PHP CodeSniffer
vendor/bin/phpcs

# Fix code with PHP CodeSniffer
vendor/bin/phpcbf
```

Arguments and options available in the official documentation: https://www.drupal.org/docs/contributed-modules/code-review-module/php-codesniffer-command-line-usage

PHP CodeSniffer package also comes with a


(Optional) Integration for PhpStorm
-----------------------------------

Set _Inspections_ as _Weak warning_.


(Optional) Integration for Git hooks
------------------------------------

Examples:
* [Pre-commit hook: PHP CodeSniffer and PHPStan for Docksal](../../scaffold-files/._git/hooks/pre-commit__docksal-phpcs-phpstan)
* [Pre-commit hook: PHP CodeSniffer and PHPStan for Docker](../../scaffold-files/._git/hooks/pre-commit__docker-phpcs-phpstan)
