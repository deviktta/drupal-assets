# Extending Drupal - Code review - PHPStan

* PHPStan documentation: https://phpstan.org/user-guide/getting-started
* Drupal documentation for PHPStan: https://www.drupal.org/docs/develop/development-tools/phpstan/getting-started

**Contents:**
1. [Installation](#step-1--installation)
2. [Configuration](#step-2--configuration)
3. [Run](#step-3--run)
4. [(Optional) Integration for PhpStorm](#-optional--integration-for-phpstorm)
5. [(Optional) Integration for Git hooks](#-optional--integration-for-git-hooks)


Step 1: Installation
--------------------

Install PhpStan Composer packages and their related extensions only for dev:
```shell
composer require --dev phpstan/phpstan phpstan/extension-installer mglaman/phpstan-drupal phpstan/phpstan-deprecation-rules
```

* `phpstan/phpstan`: PHPStan main package.
* `mglaman/phpstan-drupal`: PHPStan package with rules for Drupal.
* `phpstan/phpstan-deprecation-rules`: PHPStan package with rules for deprecated code.
* `phpstan/extension-installer`: PHPStan package for autoconfigure PHPStan extension.


Step 2: Configuration
---------------------

Create a new file inside the Drupal project root directory, name it `phpstan.neon` and configure it with your needs:
* Sample file: [phpstan.dist.neon](../../scaffold-files/phpstan.neon.dist)
* PhpStan standard file name for distribution: `phpstan.dist.neon`


Step 3: Run
-----------

Now, PHPStan will detect automatically our `phpstan.neon` file; it's enough by running PHPStan from the CLI:
```shell
vendor/bin/phpstan
```

Arguments and options available in the official documentation: https://phpstan.org/config-reference


(Optional) Integration for PhpStorm
-----------------------------------

Set _Inspections_ as _Weak warning_.


(Optional) Integration for Git hooks
------------------------------------

Examples:
* [Pre-commit hook: PHP CodeSniffer and PHPStan for Docksal](../../scaffold-files/._git/hooks/pre-commit__docksal-phpcs-phpstan)
* [Pre-commit hook: PHP CodeSniffer and PHPStan for Docker](../../scaffold-files/._git/hooks/pre-commit__docker-phpcs-phpstan)
