# Extending Drupal - Drupal - Menu API

**Providing module-defined menu links**
- *.links.menu.yml: Defines menu links for navigation within the site's menus.

**Providing module-defined local tasks**
- *.links.task.yml: Defines task links (tabs) for local tasks related to specific entities or administrative pages.

**Providing module-defined local actions**
- *.links.action.yml: Use actions to define local operations such as adding new items to an administrative list (menus, contact categories, etc).

**Providing module-defined contextual links**
- *.links.contextual.yml: Use contextual links to provide contextual operations to users around common Drupal objects appearing on the frontend.
