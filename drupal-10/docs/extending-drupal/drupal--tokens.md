# Extending Drupal - Drupal - Tokens

**Contents:**
1. Introduction to Drupal tokens
2. Creating custom Drupal tokens
   1. Implement `hook_tokens()`
   2. (Optional) Implement `hook_token_info()`
3. Adding context data to our custom token


Step 1: Introduction to Drupal tokens
-------------------------------------

Drupal tokens are string placeholders in the form of `[type:token]` (such as `[current-user:name]`) that automatically
fills dynamic data. Tokens are grouped by the types; then, we can define as many tokens as we want and group
them as we consider.


Step 2: Creating custom Drupal tokens
-------------------------------------

In order to create Drupal tokens, we will need to implement `hook_tokens()` to define the logic for our custom token;
optionally but recommended, we can implement `hook_token_info()` to provide a human name and description for our custom
token, as this information will be displayed on text editing screens (using the action link _Browse available tokens_).

## Implement `hook_tokens()`

```php
/**
 * Implements hook_tokens().
 */
function my_module_token_tokens($type, $tokens, array $data, array $options, \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if ($type == 'custom-type') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'custom-token':
          $state = \Drupal::state();
          // Update token cache dependencies.
          $bubbleable_metadata->addCacheableDependency($state);
          // Do our stuff with our custom token.
          $replacements[$original] = $state->get('email');
          break;
       default:
        break;
      }
    }
  }

  return $replacements;
}
```

## Implement `hook_token_info()`

```php
/**
 * Implements hook_token_info().
 */
function custom_token_token_info() {
  $custom_token_type = 'custom-type';

  // Description for our custom token group.
  $custom_token_type_info = [
    'name' => t('Custom type'),
    'description' => t('Custom type grouping for tokens'),
  ];

  // Description for our custom token.
  $custom_type_tokens = [];
  $custom_type_tokens['custom-token'] = [
    'name' => t('Custom token'),
    'description' => t('Custom token to do some stuff'),
  ];

  return [
    'types' => [
      $custom_token_type => $custom_token_type_info, 
    ],
    'tokens' => [
      $custom_token_type => $custom_type_tokens,
    ],
  ];
}
```


Step 3: Adding context (aka as $data) to our custom token
---------------------------------------------------------

In some cases, we will need some context data in our tokens, such as the current node using the token. It's
advised to review Drupal's core token implementation (for example, for node or terms). In order to pass the context
data, we won't need to define a custom token group, as we need to group our token with their corresponding group.

For example, if we need the pass the node using our token, we will group our token into the node's group: updating
the last example, in `hook_tokens()` we need to check if _$node_ entity has been passed; in `hook_token_info()`, we don't
create any group and attach it _node_ token's group.


## Implement `hook_tokens()`

```php
/**
 * Implements hook_tokens().
 */
function my_module_token_tokens($type, $tokens, array $data, array $options, \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if ($type == 'custom-type' && !empty($data['node'])) {
    /** @var \Drupal\node\Entity\Node $node */
    $node = $data['node'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'custom-token':
          // Update token cache dependencies.
          $bubbleable_metadata->addCacheableDependency($node);
          // Do our stuff with our custom token.
          $replacements[$original] = $node->id();
          break;
       default:
        break;
      }
    }
  }

  return $replacements;
}
```


## Implement `hook_token_info()`

```php
/**
 * Implements hook_token_info().
 */
function custom_token_token_info() {
  // Description for our custom token.
  $node_tokens = [];
  $node_tokens['custom-token'] = [
    'name' => t('Custom token'),
    'description' => t('Custom token to do some stuff'),
  ];

  return [
    'tokens' => [
      'node' => $node_tokens,
    ],
  ];
}
```
