# Extending Drupal - E-mail - Sending e-mails with Drupal

_This article was written for Drupal 9; to be tested in Drupal 10._

**Contents:**
- [Initial setup](#initial-setup)
- [Sending e-mails](#sending-e-mails)
  - [PHP mail function](#php-mail-function)
  - [SMTP](#smtp)
- [Testing e-mails](#testing-e-mails)
  - [Mailhog: a local testing SMTP server](#mailhog--a-local-testing-smtp-server)
  - [Mail debugger, a Drupal contrib module](#mail-debugger--a-drupal-contrib-module)


## Initial setup

Mailsystem module is the main Drupal module in charge of the mail setup: within this module we will be able to configure the way we send e-mails (SMTP, PHP mailer, ...) and the formatter plugin to render the e-mail content along with the theme. Also, we can define specific setup per module (the ones implementing hook_mail).
```ssh
composer require drupal/mailsystem
drush pm:enable mailsystem
```


## Sending e-mails

### PHP mail function

A popular way to send e-mails is using the in-built [PHP mail function](https://www.php.net/manual/en/function.mail.php) shipped with PHP. For this, [Symfony Mailer Drupal module](https://www.drupal.org/project/symfony_mailer) is required. For the record, prior to Drupal 9, the [Swift Mailer Drupal module](https://www.drupal.org/project/symfony_mailer) was widely used until it was deprecated in favor of the Symfony Mailer.

Symfony Mailer Drupal module integrates the [Mailer component for Symfony](https://symfony.com/doc/current/mailer.html), which introduces the transports, defining the way we send mails, that could be via PHP mailer, DSN, SMTP and so on. E-mail policies need to be configured as well in order to set priorities.

IMPORTANT: All the e-mail settings need to be configured in the php.ini files, and this it's not covered in this guide.

**Step 1/3 - Download and install:**
```ssh
composer require drupal/symfony_mailer
drush pm:enable symfony_mailer
```

**Step 2/3 - Configure Symfony Mailer module:**

To create a new _e-mail transport_ using the PHP mail function, navigate to the Symfony Mailer settings page (/admin/config/system/mailer) > Transports tab; select Native from the dropdown and click Add transport; give it a name and save it. Set our new transport as default and, for this guide, all the other transports can be safely removed.

Then, configure e-mail policies: navigate to the Symfony Mailer settings page (/admin/config/system/mailer) > Policy tab. In this page, we can configure e-mail policies with set specific values (to, from, subject...) for each our mailing cases; for this guide, they are not needed, this way we will remove all policies except _All_ policy.

**Step 3/3 - Configure Mailsystem module to use PHP mailer:**

Now, we have a working implementation for the PHP mailer; go to the Mailsystem settings page (/admin/config/system/mailsystem) and select Default php mailer as Sender.

Now it's time to test our e-mails.


### SMTP

A very common setup to send e-mails is through an SMTP server. For this, [SMTP Drupal module](https://www.drupal.org/project/smtp) is required. Even though Symfony Mailer Drupal module allows to create SMTP transports, we recommend to use SMTP Drupal.

**Step 1/3 - Download and install:**
```ssh
composer require drupal/smtp
drush pm:enable smtp
```

**Step 2/3 - Configure SMTP module:**

Once we got the module installed, navigate to the SMTP settings page (/admin/config/system/smtp) to add the connection details (see the Settings samples section). You can override this config in settings.php in order to put confidential info.

**Step 3/3 - Configure Mailsystem module to use SMTP:**

Now, we have a working SMTP implementation; go to the Mailsystem settings page (/admin/config/system/mailsystem) and select SMTP Mailer as Sender.

Now it's time to test our e-mails.


## Testing e-mails

### Mailhog: a local testing SMTP server

Mailhog sets a fake SMTP server for testing purposes, allowing to send e-mails and then being able to see them.

```text
# SMTP settings for a default Docksal project

SMTP server: mail
SMTP port: 1025
Use encrypted protocol: No
Enable TLS encryption automatically: On
```

```text
# SMTP settings for a default Mailhog Docker container

SMTP server: mailhog
SMTP port: 1025
Use encrypted protocol: No
Enable TLS encryption automatically: On
```


### Mail debugger: a Drupal contrib module

[Mail debugger module](https://www.drupal.org/project/mail_debugger) is a simple and straight-forward way to test e-mails.
```ssh
composer require drupal/mail_debugger
drush pm:enable mail_debugger
```

After installing it, just access the form page (/admin/config/development/mail_debugger) to send a test e-mail.

It's also possible to have a specific mail configuration (sender + formatter + theme) for Mail debugger module (/admin/config/system/mailsystem).
