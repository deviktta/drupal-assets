# Extending Drupal - Forms - AJAX

## Example: one field content handles _disabled_ property of another field

Example context: our form has 1 e-mail field and a submit button. By default, submit button will be disabled until a valid e-mail is filled in; we will use change event to detect updates on e-mail field. 

Approach:
   - First, we will update our form within the `buildForm()` function using `$form_state->getValues()` to retrieve live form data.
   - Because of the way Drupal handles the field replacement, we will need to create a parent <div> surrounding our button for Drupal to replace it.
   - After defining the `['#ajax']` properties, we code the new AJAX callback function:
     - In this case we return the container element, which owns our field; but we could use `['#prefix']` or `['#suffix']` to set up the parent div, and in that case it'd enough to return the field itself.
     - `['#ajax']['wrapper']` value must match the HTML id of the updated field container div; consider customizing ids via `['#attributes']['id']`.

```php
public function buildForm(array $form, FormStateInterface $form_state) {
    $form['email'] => [
        '#type' => 'email',
        '#title' => $this->t('E-mail'),
        '#ajax' => [
            'callback' => '::ajaxCallbackEmail',
            'disable-refocus' => TRUE,
            'event' => 'change',
            'wrapper' => 'wrapper-email',
        ]
    ]

    $email = $form_state->getValue('email');
    $form['wrapper_email_submit'] = [
        '#type' => 'container',
        '#attributes' => [
            'id' => 'wrapper-email',
        ],
    ];
    $form['wrapper_email_submit']['submit'] = [
        '#type' => 'submit',
        '#label' => $this->t('Submit'),
        '#disabled' => empty($email),
    ];
}

public function ajaxCallbackEmail($form, FormStateInterface $form_state) {
    return $form['wrapper_email_submit'];
}
```
