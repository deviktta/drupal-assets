# Extending Drupal - Search - Search API

_This article was written for Drupal 9; to be tested in Drupal 10._

* Drupal documentation for Search API: https://www.drupal.org/docs/8/modules/search-api/getting-started

**Contents:**
1. Introduction to Search API
2. Install Search API
3. Configure Search API
   1. Database back-end


Step 1: Introduction to Search API
----------------------------------

The Search API module provides a decoupled way to store data based on server and indexes, usually meant to integrate with high-performance search engines like Apache Solr.


Step 2: Install Search API
--------------------------

It belongs to Drupal core; just install it the regular way:
```shell
drush pm:enable search_api
```


Step 3: Configure Search API 
----------------------------

### Database back-end

**Install and configure Database Search:**

For this step, Search API module is required: see [how to Search API](search--search-api.md).

The Database Search (search_api_db) module provides an integration of the Database back-end with the Search API.

This module also belong to Drupal core; just install it the regular way:
```shell
drush pm:enable search_api_db
```

**Configure Database Search**

To configure this module, we will need to create a server and index for the Database back-end.
Fortunately, Drupal core ships a module meant to provide a best-practice default setup for the Database back-end.
This module is quite special, we will install (import configuration) and right away uninstall (configuration will remain).
```shell
drush pm:enable search_api_db_defaults
drush pm:uninstall search_api_db_defaults
```

Finally, index your content (_Index now_ button in the view page of your index).
