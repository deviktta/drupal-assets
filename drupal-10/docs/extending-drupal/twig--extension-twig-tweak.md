# Extending Drupal - Twig - Twig Tweak

_This article was written for Drupal 9; to be tested in Drupal 10._

For this section, Drupal [Twig Tweak module](https://www.drupal.org/project/twig_tweak) is required. 
Full cheatsheet available at Drupal.org: [Twig Tweak Cheat sheet](https://www.drupal.org/docs/contributed-modules/twig-tweak-2x/cheat-sheet).

**Dump:**
```
{{ drupal_dump(var) }}
- [$mixed...] | Use {{ drupal_dump() }} to output all variables in current template.
```

**Entity:**
```
{{ drupal_entity('node', 123, 'teaser') }}
- $entity_type
- [$entity_id]
- [$entity_view_mode]
- [$entity_langcode]
- [$is_access_check]
```

**Blocks:**
```
# Plugin block (PHP class with a special annotation)
{{ drupal_block($plugin_id, $block_options}) }}

# Configuration block (listed in /admin/structure/block)
{{ drupal_entity('block', $block_id) }}

# Content block (listed in /admin/structure/block/block-content)
{{ drupal_entity('block_content', $content_block_id) }}
```

**Forms:**
```
{{ drupal_form('Drupal\\search\\Form\\SearchBlockForm') }}
- $form_id
- [$form_constructors...]
```

**Views:**
```
{{ drupal_view('who_s_new', 'block_1') }}
- $view_machine_name
- $view_display_id = 'default'
- [$mixed...]
```

**URL:**
```
{{ drupal_url('node/1', {query: {foo: 'bar'}, fragment: 'example', absolute: true}) }}
- $link_or_internal_path
- [$url_options]
- [$is_access_check]
```

**Links:**
```
{{ drupal_link('View'|t, 'node/1', {attributes: {target: '_blank'}}) }}
- $link_text
- $link_or_internal_path
- [$link_options]
- [$is_access_check]
```

**Menus:**
```
{{ drupal_menu('main') }}
- $menu_machine_name
- [$menu_initial_level]
- [$menu_max_depth]
- [$is_expanded]
```

**Config:**
```
{{ drupal_config('system.site', 'name') }}
- $config_name
- $config_key
```

**Title:**
```
{{ drupal_title() }}
```

**Status messages:**
```
{{ drupal_messages() }}
```

**Breadcrumb:**
```
{{ drupal_breadcrumb() }}
```


