# Development setup - ddev


## Command-Line:

**Project management:**
- Check logs: `ddev logs --service=<SERVICE_NAME>`
- Docker project restart: `ddev restart [<SERVICE_NAME>]`
- Summary of your project’s configuration: `ddev describe`
- Open shell on a service: `ddev ssh --service=<SERVICE_NAME>`

**Service - database:**
- Database cli: `ddev mysql`
- Database creation (same configuration as `db`): `ddev import-db --database=backend --file=backend.sql.gz`
- Database import: `ddev import-db --file=dumpfile.sql.gz`
- Database export: `ddev export-db --file=dumpfile.sql.gz --gzip --database=db`

**Tools:**
- Drush: `ddev drush <DRUSH_COMMAND>`
- PhpMyAdmin:
  - `ddev get ddev/ddev-phpmyadmin`
  - URL: ?
- Adminer:
  - `ddev get ddev/ddev-adminer`
  - URL: ?
- Mailhog:
  - ?
  - URL: ?


## New Project

* Clone or create the code for your project.
* `cd` into the project directory and run `ddev config` to initialize a DDEV project.
* Run `ddev start` to spin up the project.
