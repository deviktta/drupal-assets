# Development setup - Docksal


## Docksal

**Project management:**
- Check logs: `fin logs [<SERVICE_NAME>]`
- Docker project restart: `fin project restart [<SERVICE_NAME>]`
- Docker container reset (useful when overriding service settings): `fin project reset [<SERVICE_NAME>]`
- Open shell on a service: `fin bash [<SERVICE_NAME>]`

**Service - database:**
- Database cli: `fin db cli [QUERY]`
- Database import: `fin db import <PATH_TO_SQL_FILE> --progress`
- Database import (compressed with gzip): `zcat < <PATH_TO_SQL_GZIPPED_FILE> | fin db import`
- Database export: `fin db dump <PATH_TO_SQL_FILE>`
- Database ops: `fin db list|create|drop|truncate`

**Service URLs:**
- PhpMyAdmin: http://pma-<VIRTUAL_HOST>
- Apache Solr: http://solr.<VIRTUAL_HOST>/solr
- Mailhog: http://mail.<VIRTUAL_HOST>

**Links:**
- Docksal overriding examples at Github: [https://github.com/docksal/docksal/tree/develop/examples](https://github.com/docksal/docksal/tree/develop/examples)
