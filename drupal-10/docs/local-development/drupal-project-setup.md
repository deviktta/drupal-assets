# Local development - Drupal project setup: Docker Compose + DDEV


**Docker and Docker Compose:**
- [ ] Follow official guide https://docs.docker.com/engine/install/

**DDEV:**
- [Cheatsheet: Drupal CLI (DDEV)](ddev.md)
- [ ] Include custom commands [/drupal-10/scaffold-files/.ddev/commands/web](/drupal-10/scaffold-files/.ddev/commands/web).
- [ ] If necessary, override DDEV configuration file.

**PhpStorm:**
- [ ] Plugins:
  - DDEV integration
  - Symfony Support
  - Drupal
  - Drupal Symfony Bridge
  - PHP Annotations
  - PHP Toolbox
  - deep-assoc-completion
  - GNU GetText Files Support (*.po)
  - .env files support
  - String manipulation
- [ ] General configuration
  - Make _.git_ directory visible: Navigate to _Settings > Editor > File types_ -> Open _Ignored files and folders_ tab -> Remove _.git_ from the list.
  - Review PHP path mappings, navigate to _Settings > PHP | Path mappings_.
  - Create a new CLI interpreter using Docker Compose option.
- [ ] PHPStan
  - See _Integrate PHPStan in PhpStorm_ in [Extending Drupal - PHPStan](../extending-drupal/code-review--phpstan.md#optional-integration-for-phpstorm)
- [ ] PHP CodeSniffer
  - See _Integrate Php CodeSniffer_ in [Extending Drupal - Php CodeSniffer](../extending-drupal/code-review--php-codesniffer.md)

**Git:**
- [ ] Add _commit-msg_ hook to follow Conventional Commits [/git/hooks/commit-msg--conventional-commits](/git/hooks/commit-msg--conventional-commits).
- [ ] Add _pre-commit_ hook [/git/hooks/pre-commit--ddev--drupal-validate](/git/hooks/pre-commit--ddev--drupal-validate).
- [ ] Add _pre-push_ hook [/git/hooks/pre-push--ddev--phpunit](/git/hooks/pre-push--ddev--phpunit)

**Miscellaneous apps:**
- [ ] Flameshot
- [ ] Git Kraken
- [ ] Postman

**Drupal configuration**
- [ ] Check directory config (outside document root): /config/default @todo review Amer Sports
- [ ] Check directory `private` (outside document root): directory perm 750, files perm 640.
- [ ] Check directory `files`: directory perm 755, files perm 644.
- [ ] Review `local.setting.php` (custom project configuration, see [development.settings.local.php](/drupal-10/scaffold-files/web/sites/default/development.settings.local.php)).
- [ ] Review `services.yml` (see [development.services.yml](/drupal-10/scaffold-files/web/sites/default/development.services.yml))
- [ ] Import database (using [Drush](/drupal-10/docs/basics/drush.md)).
- [ ] Copy static files (using [Drush](/drupal-10/docs/basics/drush.md)).
- [ ] Run deploy commands (usually Composer + drush updb + drush cim).
