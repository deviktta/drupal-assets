#!/usr/bin/env bash

# File: import-drupal-translations
# Version: 20231119

#----------------------- Initialization
## Abort if anything fails
set -e
langs=("de" "en" "it" "fr" "es")

## Arguments detection
if [[ $# -gt 1 ]] ; then
  env="$1"
  dir="$2"
fi

#----------------------- Execution
for lang in "${langs[@]}"
do
  file="${lang}.po"
  fullDir=$dir$file
  echo "Import '${lang}' translations in '${fullDir}' at ${env}? [Y/N]"
  read -r answer
  case $answer in
      Y|y )
        break;;
      N|n|default )
        continue;;
  esac
done
