<?php

// phpcs:ignoreFile

/**
 * @file
 * Drupal site-specific configuration file.
 *
 * @version 1.0.1
 */

// DDEV.
if (getenv('IS_DDEV_PROJECT') == 'true' && file_exists(__DIR__ . '/settings.ddev.php')) {
  include __DIR__ . '/settings.ddev.php';
}

// Drupal assertions.
ini_set('zend.assertions', 1);

// Container services' yaml.
$settings['container_yamls'][] = 'sites/development.services.yml';

// Errors and logging.
error_reporting(E_ALL);
//error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('log_errors', TRUE);
ini_set('html_errors', TRUE);
ini_set('display_errors', "1");
ini_set('display_startup_errors', TRUE);
$config['system.logging']['error_level'] = 'verbose';

// Performance.
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

// Cache.
$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['discovery_migration'] = 'cache.backend.memory';
$settings['cache']['bins']['page'] = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';

// Allow test modules and themes to be installed.
$settings['extension_discovery_scan_tests'] = TRUE;

// Enable access to rebuild.php script.
$settings['rebuild_access'] = FALSE;

// Access control for update.php script.
$settings['update_free_access'] = FALSE;

// Skip file system permissions hardening.
$settings['skip_permissions_hardening'] = TRUE;

// Database connection settings.
$databases['default']['default'] = [
  'database' => getenv('MYSQL_DATABASE'),
  'prefix' => '',
  'username' => getenv('MYSQL_USER'),
  'password' => getenv('MYSQL_PASSWORD'),
  'host' => getenv('MYSQL_HOST'),
  'port' => '3306',
  'driver' => 'mysql',
  'namespace' => 'Drupal\\mysql\\Driver\\Database\\mysql',
  'autoload' => 'core/modules/mysql/src/Driver/Database/mysql/',
  'init_commands' => [
    'isolation_level' => 'SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED',
  ],
];

// Configuration when reverse proxy is in place.
//if (PHP_SAPI !== 'cli') {
//    $settings['reverse_proxy'] = TRUE;
//    $settings['reverse_proxy_addresses'] = [$_SERVER['REMOTE_ADDR']];
//    $settings['reverse_proxy_trusted_headers'] = \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_FOR | \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_HOST | \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_PORT | \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_PROTO | \Symfony\Component\HttpFoundation\Request::HEADER_FORWARDED;
//    if (
//        isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' &&
//        !empty($settings['reverse_proxy']) && in_array($_SERVER['REMOTE_ADDR'], $settings['reverse_proxy_addresses'])
//    ) {
//        $_SERVER['HTTPS'] = 'on';
//        $_SERVER['SERVER_PORT'] = 443;
//    }
//}

// Hash salt.
$settings['hash_salt'] = 'WSwxSIzzqrBEA98wSWtJBYOT-MeBzbM9cmkALiKm-d-8TC0mz-ksCzMmx15Y62vbzQgwP1u3ug';

// Config settings.
$settings['config_sync_directory'] = '../config/default';
$settings['config_exclude_modules'] = ['devel'];

// File system settings.
$settings['file_public_path'] = 'sites/default/files';
$settings['file_private_path'] = '../private'; // Outside document root.
$settings['file_temp_path'] = '/tmp'; // Outside document root.

// Trusted host patterns.
$settings['trusted_host_patterns'] = ['.*'];

/**
 * Project custom settings.
 */
