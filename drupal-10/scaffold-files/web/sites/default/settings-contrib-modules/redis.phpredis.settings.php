<?php

# Redis
$settings['cache']['default'] = 'cache.backend.redis';
$settings['cache']['bins']['discovery_migration'] = 'cache.backend.redis';
$settings['redis.connection']['interface'] = 'PhpRedis';
$settings['redis.connection']['host'] = 'redis';
$settings['redis.connection']['password'] = 'redis';
