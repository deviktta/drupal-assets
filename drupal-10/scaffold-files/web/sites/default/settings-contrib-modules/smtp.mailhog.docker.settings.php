<?php

# SMTP config.
$config['smtp.settings'] = [];
$config['smtp.settings']['smtp_on'] = TRUE;
$config['smtp.settings']['smtp_host'] = 'mailhog';
$config['smtp.settings']['smtp_port'] = 1025;
$config['smtp.settings']['smtp_username'] = '';
$config['smtp.settings']['smtp_password'] = '';
$config['smtp.settings']['smtp_protocol'] = 'tls';