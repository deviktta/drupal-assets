# Drupal 10 - my_module.routing.yml

#admin #form #permissions
```yml
module_name.example:
  path: '/admin/example'
  defaults:
    _form: '\Drupal\module_name\Form\ExampleForm'
    _title: 'Example'
  requirements:
    _permission: 'access administration pages'
  options:
    _admin_route: TRUE
```