# Git

**Config:**
- [Git config template](git-config-template.sh)

**Hooks:**
- [commit-msg: Conventional Commits](hooks/commit-msg--conventional-commits)
- [pre-commit: Composer, PHP, Drupal, Twig, Yaml](hooks/pre-commit--composer-php-drupal-twig-yaml)
- [pre-push: PHPUnit tests](hooks/pre-push--phpunit-phpstan)
