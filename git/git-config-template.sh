#!/bin/sh
# Author: deviktta@protonmail.com
# Description: Git project initial configuration

git config user.name "<USERNAME>"
git config user.email "<EMAIL>"
git config user.signingkey "<GPG_KEY_ID>"
git config commit.gpgsign true
git config core.filemode false
git config push.followTags true
git config pull.rebase true
