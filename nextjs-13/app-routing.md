# NextJS 13 - App Routing


## Features:

- All components are React server components by default.

**Top-level folders**

- `app`: App Router
- `pages`: Pages Router
- `public`: Static assets to be served
- `src`: Optional application source folder

**Files**

NextJS:
- `next.config.js`: Configuration file for Next.js
- `middleware.ts`: Next.js request middleware
- `instrumentation.ts`: OpenTelemetry and Instrumentation
- `.env`: Environment variables
- `.env.local`: Local environment variables
- `.env.production`: Production environment variables
- `.env.development`: Development environment variables
- `.next-env.d.ts`: TypeScript declaration file for Next.js

Ecosystem:
- `package.json`: Project dependencies and scripts
- `.gitignore`: Git files and folders to ignore
- `tsconfig.json`: Configuration file for TypeScript
- `jsconfig.json`: Configuration file for JavaScript
- `.eslintrc.json`: Configuration file for ESLint

Routing:

- `layout(.js .jsx .tsx)`: Layout
- `page(.js .jsx .tsx)`: Page
- `loading(.js .jsx .tsx)`: Loading UI
- `not-found(.js .jsx .tsx)`: Not found UI
- `error(.js .jsx .tsx)`: Error UI
- `global-error(.js .jsx .tsx)`: Global error UI
- `route(.js .ts)`: API endpoint
- `template(.js .jsx .tsx)`: Re-rendered layout
- `default(.js .jsx .tsx)`: Parallel route fallback page


## Routing & pages:

- Routing:
    - Next.js uses a file-system based router where folders are used to define routes.
    - A Dynamic Segment can be created by wrapping a folder's name in square brackets: [folderName]. For example, [id] or [slug].
    - Pages:
        - A special `page.js` file is used to make route segments publicly accessible. You can nest folders inside each other.
        - You can define a layout by default exporting a React component from a `layout.js` file. Layouts can be nested.
        - The root layout (required) is defined at the top level of the app directory and applies to all routes.
        - Templates are similar to layouts in that they wrap each child layout or page. Unlike layouts that persist across routes and maintain state, templates create a new instance for each of their children on navigation.


## Modifying the <head>:

In the app directory, you can modify the `<head>` HTML elements such as title and meta using the built-in SEO support.

Metadata can be defined by exporting a metadata object or generateMetadata function in a `layout.js` or `page.js` file.


```tsx
export async function generateMetadata() {
    const metadata: any = {
        title: 'My title'
    }
    return metadata;
}
```

```tsx
import { Metadata } from 'next'
 
export const metadata: Metadata = {
  title: 'Next.js',
}
 
export default function Page() {
  return '...'
}
```


## Data fetching

The following way of data fetching applies for Pages and Layouts, since they are all React server components.

1. We need to make our component asynchronous by adding `async` to the export function definition.
2. Now, we are able to use fetch function:
```tsx
type Repository = {
    id: number;
}
export default async function Page() {
    const res = await fetch('https://api.github.com/repos/vercel/next.js');
    const data = await res.json();
    return <h1>{data.id}</h1>;
}
```

**Caching:**
By default, responses are cached in our application; use `{ cache: 'no-store' }` fetch function parameter to make an API call on every request.
Another parameter to cache response for 5 seconds: `{ next: { revalidate: 5 } }`

**Multiple API call:**
```tsx
async function getDataOne() { 
    /* ... */
}
async function getDataTwo() {
    /* ... */
}
export default async function Page() {
    const [one, two] = await Promise.all([getDataOne(), getDataTwo()]);
    /* ... */
}
```

