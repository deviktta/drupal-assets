# PHP cheatsheet

- [Operators](#operators)
    - [Ternary operator ?: (+PHP 5.4)](#ternary-operator--php-54)
    - [Null coalescing operator ?? (+PHP 7.0)](#null-coalescing-operator--php-70)
    - [Null coalescing assignment operator ??= (+PHP 7.0)](#null-coalescing-assignment-operator--php-70)
    - [Nullsafe operator ?-> (+PHP 8.0)](#nullsafe-operator-php-80)

- [Features](#features):
  - [Late Static Bindings: self vs static (+PHP 5.3)](#late-static-bindings-self-vs-static-php-53)
  - [Constructor property promotion (+PHP 8.0)](#constructor-property-promotion-php-80)
  - [Named arguments (+PHP 8.0)](#named-arguments-php-80)
  - [Enumerations (+PHP 8.1)](#enumerations-php-81)
  - [Pure Intersection Types (+PHP 8.1)](#pure-intersection-types-php-81)
  - [Other:](#other)
    - Union types (+PHP 8.0)
    - _mixed_ type (+PHP 8.0)
    - _match_ expression (+PHP 8.0)
    - Non-capturing catch (+PHP 8.0)
    - Final class constants (+PHP 8.1)
    - Readonly classes (+PHP 8.2)
    - Deprecate dynamic properties (+PHP 8.2)

- [Error reporting](#error-reporting)

- [Guzzle requests](#guzzle-requests)
    - [form_params](#formparams)
    - [body](#body)
    - [multipart](#multipart)
    - [json](#json)

## Operators

### Ternary operator ?: (+PHP 5.4)

The expression `(expr1) ? (expr2) : (expr3)` evaluates to expr2 if expr1 evaluates to true, and expr3 if expr1 evaluates to false. It is possible to leave out the middle part of the ternary operator.
Expression `expr1 ?: expr3` evaluates to the result of expr1 if expr1 evaluates to true, and expr3 otherwise. expr1 is only evaluated once in this case.

```php
// Example usage for: Ternary Operator
$action = (empty($_POST['action'])) ? 'default' : $_POST['action'];

// The above is identical to this if/else statement
if (empty($_POST['action'])) {
    $action = 'default';
} else {
    $action = $_POST['action'];
}
```


### Null coalescing operator ?? (+PHP 7.0)

The expression `(expr1) ?? (expr2)` evaluates to expr2 if expr1 is null, and expr1 otherwise. In particular, this operator does not emit a notice or warning if the left-hand side value does not exist, just like isset(). This is especially useful on array keys.

```php
// Example usage for: Null Coalesce Operator
$action = $_POST['action'] ?? 'default';

// The above is identical to this if/else statement
if (isset($_POST['action'])) {
    $action = $_POST['action'];
} else {
    $action = 'default';
}
```


### Null coalescing assignment operator ??= (+PHP 7.0)

Despite ?? coalescing operator being a comparison operator, coalesce equal or ??= operator is an assignment operator. If the left parameter is null, assigns the value of the right parameter to the left one. If the value is not null, nothing is made.
```php
// The following lines are doing the same
$test['key'] = $test['key'] ?? 'value';
$test['key'] ??= 'value';
```


### Nullsafe operator (+PHP 8.0)

Instead of null check conditions, you can now use a chain of calls with the new nullsafe operator. When the evaluation of one element in the chain fails, the execution of the entire chain aborts and the entire chain evaluates to null.
```php
// PHP >=8.0
$country = $session?->user?->getAddress()?->country;

// PHP <8.0
$country =  null;
if ($session !== null) {
  $user = $session->user;
  if ($user !== null) {
    $address = $user->getAddress();
    if ($address !== null) {
      $country = $address->country;
    }
  }
}
```

## Features

### Late Static Bindings: _self_ vs _static_ (+PHP 5.3)

[PHP's Late Static Bindings](https://www.php.net/manual/en/language.oop5.late-static-bindings.php#language.oop5.late-static-bindings.self)

- `self` refers to the class where the keyword is actually written.
- `static` refers to the class resolved using runtime information (which means the _real_ caller class).


### Constructor property promotion (+PHP 8.0)

Less boilerplate code to define and initialize properties.
```php
// PHP >=8.0
class Point {
    public function __construct(
        public float $x = 0.0,
        public float $y = 0.0,
        public float $z = 0.0,
    ) {}
}

// PHP <8.0
class Point {
    public float $x;
    public float $y;
    public float $z;
 
    public function __construct(
        float $x = 0.0,
        float $y = 0.0,
        float $z = 0.0,
    ) {
        $this->x = $x;
        $this->y = $y;
        $this->z = $z;
    }
}
```


### Named arguments (+PHP 8.0)

Named arguments allow passing arguments to a function based on the parameter name, rather than the parameter position (the order in which the named arguments are passed does not matter).
```php
// Using positional arguments:
array_fill(0, 100, 50);
 
// Using named arguments (+PHP 8.0):
array_fill(value: 50, num: 100, start_index: 0);
```


### Enumerations (+PHP 8.1)

Use enum instead of a set of constants and get validation out of the box.
```php
enum Status
{
    case Draft;
    case Published;
    case Archived;
}
function acceptStatus(Status $status) {...}
```


### Readonly properties (+PHP 8.1)
Readonly properties cannot be changed after initialization, i.e. after a value is assigned to them.
```php
class BlogData
{
    public readonly Status $status;
   
    public function __construct(Status $status) 
    {
        $this->status = $status;
    }
}
```


### Pure Intersection Types (+PHP 8.1)
Use intersection types when a value needs to satisfy multiple type constraints at the same time.
```php
// PHP >=8.1
function count_and_iterate(Iterator&Countable $value) {}

// PHP <8.1
function count_and_iterate(Iterator $value) {
    if (!($value instanceof Countable)) {
        throw new TypeError('value must be Countable');
    }
    // ...
}
```


### Other

- Union types (+PHP 8.0): `private int|float $number;`.
- _mixed_ type (+PHP 8.0): A type of mixed would be equivalent to `array|bool|callable|int|float|null|object|resource|string`.
- _match_ expression (+PHP 8.0): Match branches only support single-line expressions and do not need a `break;` statement + Match does strict comparisons + Match is an expression rather than a conditional statement (like `switch`).
- Non-capturing catch (+PHP 8.0): It is possible to capture an exception without storing the exception in a variable.
- Final class constants (+PHP 8.1): It is possible to declare final class constants, so that they cannot be overridden in child classes.
- Readonly classes (+PHP 8.2): Doing so will implicitly mark all instance properties of a class as `readonly`.
- Deprecate dynamic properties (+PHP 8.2): The creation of dynamic properties is deprecated to help avoid mistakes and typos, unless the class opts in by using the `#[\AllowDynamicProperties]` attribute.


## Error reporting

```php
// Turn on
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);

// Turn off
error_reporting(0);
```


## Guzzle requests
More information at [Guzzle docs](https://docs.guzzlephp.org/en/stable/request-options.html).

### form_params

Used to send an application/x-www-form-urlencoded POST request. Sets the Content-Type header to application/x-www-form-urlencoded when no Content-Type is present.
```php
$client->request('POST', '/post', [
    'form_params' => [
        'foo' => 'bar'
    ]
]);
```


### body

The body option is used to control the body of an entity enclosing request (e.g., PUT, POST, PATCH).
```php
$client->request('PUT', '/put', ['body' => $resource]);
```


### multipart

Sets the body of the request to a multipart/form-data form.
```php
$client->request('POST', '/post', [
    'multipart' => [
        [
            'name'     => 'foo',
            'contents' => 'data',
            'headers'  => ['X-Baz' => 'bar']
        ],
    ]
]);
```


### json

Used to send an application/json request. It JSON encodes data as the body of the request, setting also the Content-Type header.
```php
$client->request('PUT', '/put', ['json' => ['foo' => 'bar']]);
```
This request option does not support customizing the Content-Type header or any of the options from PHP's json_encode() function.
