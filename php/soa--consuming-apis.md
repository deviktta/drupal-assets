# PHP - Service-Oriented Architecture - Consuming APIs


## Building an API Client service

**Strategy**
- Build a service class to handle all the connection logic, including token management or API call monitoring.
- This class should've injected an HTTP Client factory to provide a new customized HTTP Client on every request.
- The API Client must not be injected everywhere, as it only should live in dedicated services that consume the API.

**Properties:**
- There will be only 2 public methods in the class:
  - `request()` method, a wrapper for Guzzle's request method:
    - Method signature should be extended, for example to determine a type of token when calling our API.
    - Within this method will log & monitor the performance of the API calls.
    - In order to follow best-practices, set `http-errors`'s Guzzle option to _true_ for the API call to throw dedicated exceptions.
    - This method must return the HTTP response `\Psr\Http\Message\ResponseInterface` or throw a Guzzle exception `\GuzzleHttp\Exception\GuzzleException`

  - `getErrorsByException()` method, using to get HTTP requests errors:
    - It doesn't matter which standard the API follows (like JSON:API), that's the proper method to homogenize error handling.

**Example:**
* Drupal module: [API consumer helper](https://gitlab.com/deviktta/drupal_api_consumer_helper)
