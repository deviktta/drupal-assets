# Symfony Component: Doctrine

Composer package: `symfony/orm-pack`

Symfony has no database layer at all. Instead, it leverages another library called _Doctrine_, being installed in the ORM pack.


## Doctrine:

- `doctrine:database:create`: Creates the configured database.
- `doctrine:database:drop`: Drops the configured database.
- `doctrine:query:sql`: Executes arbitrary SQL directly from the command line.
- `doctrine:schema:create`: Processes the schema and either create it directly on EntityManager Storage Connection or generate the SQL output.
- `doctrine:schema:drop`: Drop the complete database schema of EntityManager Storage Connection or generate the corresponding SQL output.
- `doctrine:schema:update`: Executes (or dumps) the SQL needed to update the database schema to match the current mapping metadata.

**Doctrine Migrations:**
- `doctrine:migrations:dump-schema`: Dump the schema for your database to a migration.
- `doctrine:migrations:diff`: Generate a migration by comparing your current database to your mapping information.
- `doctrine:migrations:migrate`: Execute a migration to a specified version or the latest available version
- `doctrine:migrations:up-to-date`: Tells you if your schema is up-to-date.
- `doctrine:migrations:status`: View the status of a set of migrations.

**How to use multiple PK (Primary Key)**
Include `#[ORM\Id]` attribute on multiple class properties.
