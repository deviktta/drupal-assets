# Symfony Component: Maker

Composer package: `symfony/maker-bundle` (development tool, use `--dev` option)

Symfony Maker helps you create empty commands, controllers, form classes, tests and more so you can forget about writing boilerplate code.


## Maker

**Basic:**
- `make:controller`: Creates a new controller class.
- `make:entity`:  Creates or updates a Doctrine entity class, and optionally an API Platform resource.
- `make:form`: Creates a new form class (optionally based on an Entity).
- `make:command`: Creates a new console command class.
- `make:validator`: Creates a new validator and constraint class.
- `make:fixtures`: Creates a new class to load Doctrine fixtures.
- `make:subscriber`: Creates a new event subscriber class.
- `make:test`: Creates a new test class.

**Other:**
- `make:registration-form`: Creates a new registration form system.
- `make:security:form-login`: Generate the code needed for the form_login authenticator.
- `make:serializer:encoder`: Creates a new serializer encoder class.
- `make:serializer:normalizer`: Creates a new serializer normalizer class.
- `make:twig-component`: Creates a twig (or live) component.
- `make:twig-extension`: Creates a new Twig extension with its runtime class.

