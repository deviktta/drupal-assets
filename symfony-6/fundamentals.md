# Symfony Fundamentals

Symfony is a set of reusable PHP components and a PHP framework for web projects.


## Symfony Components

Symfony Components are decoupled libraries for PHP applications. Battle-tested in hundreds of thousands of projects and downloaded billions of times, they're the foundation of the most important PHP projects.
- [List of Symfony Components](https://symfony.com/components)

After a fresh install, only 3 components live in the Symfony application.
```json
{
  "symfony/console": "6.3.*",
  "symfony/flex": "^2",
  "symfony/framework-bundle": "6.3.*"
}
```


### Symfony Flex

Composer package: `symfony/flex`.

Symfony Flex is a tool (technically, a Composer plugin) that simplifies the installation/removal of packages in Symfony applications. Introduced in Symfony 4, along with the new project file structure (bundles division were removed).
- Composer aliases, for example `twig` resolves to `symfony/twig-bundle`.
- Includes _recipes_, which are a Composer package custom actions pre and post package installation/removal.
  - [List of Symfony Recipes - main](https://github.com/symfony/recipes)
  - [List of Symfony Recipes - contrib](https://github.com/symfony/recipes-contrib)
  - Custom recipes by adding the URL of your recipe repository in the `extra.symfony.endpoint` config option of `composer.json` or in the `SYMFONY_ENDPOINT` env var.
  - `symfony.lock`: it keeps track of which recipes have been installed.
  - `composer recipes [vendor/package]`: list of all the recipes installed & available to update/details about a specific recipe.
  - `composer recipes:update [vendor/package]`: updates a specific recipe.
  - `composer recipes:install [vendor/package]`: installs or reinstalls recipes for already installed packages.

**Reinstalling a recipe:**
1. Remove the package reference in `symfony.lock`: let Symfony know this package hasn't any recipe installed.
2. Run `composer recipes:install`: now Symfony will install the missing recipe, updating `symfony.lock` accordingly.

### Symfony Console

Composer package: `symfony/console`.

The Console component allows you to create command-line commands.

It creates a new binary `/bin/console` through its recipe, ready to be run with: `php bin/console list`.


### Symfony Framework Bundle

Composer package: `symfony/framework-bundle`.

Provides a tight integration between Symfony components and the Symfony full-stack framework


### Other well-know components

- `symfony/event-dispatcher`
- `symfony/translation`
- `symfony/form`
- `symfony/yaml`
- `symfony/routing`
- `symfony/var-dumper`
- `symfony/filesystem`
- `symfony/mailer`
- `symfony/http-foundation `